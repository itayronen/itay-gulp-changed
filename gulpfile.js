"use strict"

let gulp = require("gulp");
let path = require("path");
let tap = require("gulp-tap");
let changed = require("./src/index.js");

gulp.task("test", () => {
    return gulp.src("./test/*.txt")
        .pipe(changed({ key: "some-key", isGroupOperation: true }))
        .pipe(tap(file => console.log(path.basename(file.path))))
        //.pipe(gulp.dest("./test/output"))
        ;
});