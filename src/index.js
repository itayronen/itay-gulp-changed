'use strict';
let ostream = require("o-stream").default;
let LocalStorage = require('node-localstorage').LocalStorage;
let localStorage = new LocalStorage('./.localStorage');

let lastRunDefaultKey = "lastRunKey";

function wasModified(fileStat, lastRun) {
	return fileStat.mtime.getTime() > lastRun;
}

function compareLastModifiedTime(sourceFile, lastRun) {
	if (!sourceFile.stat) {
		console.warn(sourceFile + " Has no stats. It will always be considered as changed.");
	}

	return !sourceFile.stat || wasModified(sourceFile.stat, lastRun);
}

module.exports = function (params) {
	params = params || {};
	params.hasChanged = params.hasChanged || compareLastModifiedTime;
	params.key = params.key || lastRunDefaultKey;
	params.isGroupOperation = params.isGroupOperation || false;

	let lastRun = localStorage.getItem(params.key) || 0;
	localStorage.setItem(params.key, Date.now());

	if (params.isGroupOperation) {
		return createGroupStream(lastRun, params.hasChanged);
	}
	else {
		return createOneByOneStream(lastRun, params.hasChanged);
	};
}

function createOneByOneStream(lastRun, hasChangedFunc) {
	return ostream.transform({
		onEntered: args => {
			if (hasChangedFunc(args.object, lastRun)) {
				args.output.push(args.object);
			}
		}
	});
}

function createGroupStream(lastRun, hasChangedFunc) {
	return ostream.transform({
		onEntered: createGroupOnEnter(lastRun, hasChangedFunc)
	});
}

function createGroupOnEnter(lastRun, hasChangedFunc) {
	let groupChanged = false;
	let files = [];

	return function (args) {
		if (groupChanged) {
			args.output.push(args.object);
			return;
		}

		files.push(args.object);

		if (hasChangedFunc(args.object, lastRun)) {
			groupChanged = true;

			for (let file of files) {
				args.output.push(file);
			}
		}
	};
}

module.exports.reset = function (options) {
	options = options || {};
	options.key = options.key || lastRunDefaultKey;

	localStorage.setItem(options.key, 0);
}